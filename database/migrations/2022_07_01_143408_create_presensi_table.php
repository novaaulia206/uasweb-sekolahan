<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePresensiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('presensi', function (Blueprint $table) {
            $table->id();
            $table->foreignId('siswa_id');
            $table->foreign('siswa_id')->references('id')->on('siswa')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('mapel_id');
            $table->foreign('mapel_id')->references('id')->on('mapel')
                  ->onDelete('cascade')->onUpdate('cascade');
            $table->enum('keterangan', ['hadir','ijin','sakit','tanpa keterangan']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('presensi');
    }
}
