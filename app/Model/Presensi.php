<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Presensi extends Model
{
    protected $table = 'presensi';
    protected $fillable = ['siswa_id','mapel_id','keterangan'];

    public function siswas()
    {
        return $this->belongsTo('App\Model\Siswa','siswa_id');
    }

    public function mapel()
    {
        return $this->belongsTo('App\Model\Mapel','mapel_id');
    }
}
